// Include gulp
var gulp = require('gulp');
var inject = require('gulp-file-include');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minify = require('gulp-cssmin');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('html', function() {
  gulp.src(['html/**/*.*'])
    .pipe(inject({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('.dist/'));
});

gulp.task('scripts', function() {
  gulp.src('js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('.dist/js'));
});

gulp.task('styles', function() {
  gulp.src(['sass/general.sass'])
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sourcemaps.write())
    .pipe(minify())
    .pipe(gulp.dest('.dist/'));
});

gulp.task('content', function() {
  gulp.src('fnt/*')
    .pipe(gulp.dest('.dist/fnt'));

  gulp.src('img/*')
    .pipe(gulp.dest('.dist/img'));

  gulp.src('api/**/*.*')
    .pipe(gulp.dest('.dist/api'));
});

gulp.task('default', ['html', 'scripts', 'content', 'styles']);