(function() {
    var form = document.getElementById('login');
    var logout = document.getElementById('logout')

    if (sessionStorage.auth) {
        form.className = 'hidden';
        logout.addEventListener('click', function() {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', 'api/sessionDestroy', true);

            xhr.onload = function() {
                if (xhr.status == 200) {
                    window.location = 'blog';
                    sessionStorage.removeItem('auth');
                }
            }

            xhr.send();
        })
    }

    else {
        logout.className = 'hidden';
        form.login.addEventListener('click', function() {
            var data = {
                name: form.name.value,
                password: form.password.value
            };

            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'api/login', true);
            xhr.setRequestHeader('Content-Type', 'application/json');

            xhr.onload = function() {
                if (xhr.responseText === '\"' + form.name.value + '\"') {
                    window.location = 'blog';
                    sessionStorage.setItem('auth', xhr.response);
                }
                else {
                    console.error('podaj prawidlowe dane logowania');
                }
            }

            xhr.send(JSON.stringify(data));
        });
    }
})();
