var submit = {}

submit.order = document.getElementById('order');
submit.orderSubmit = document.getElementById('order-submit');
submit.orderEmail = document.getElementById('order-email');

submit.orderSubmit.addEventListener('click', function sendOrder() {
    var data = {
        email: submit.orderEmail.value
    };

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'api/orders');
    if (data.email) {
        xhr.send(JSON.stringify(data));
    };

    xhr.onload = function() {
        if (xhr.response.replace(/"/g, '') === data.email) {
            submit.orderSubmit.removeEventListener('click', sendOrder);
            submit.orderSubmit.classList.add('done');
            submit.orderSubmit.innerText = 'done';
            submit.orderEmail.style.marginLeft = '-36px';
            submit.orderEmail.style.width = '0';
            submit.orderSubmit.style.width = '100%';

            var data2 = {
                subject: 'New order',
                message: submit.orderEmail.value,
                email: submit.orderEmail.value
            };

            var xhr2 = new XMLHttpRequest();
            xhr2.open('POST', 'api/contact');
            xhr2.onload = function() {};
            xhr2.send(JSON.stringify(data2));
        }
    };
});

document.getElementById('back').addEventListener('click', function() {
    window.history.back();
});