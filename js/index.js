var video = {};
video.vid = document.getElementById('video');
video.tohide = document.getElementById('hide');

video.play = function() {
    if (!video.vid.paused) {
        video.vid.pause();
    }
    else {
        video.vid.play();
    }
};

video.start = function() {
    video.vid.addEventListener('click', video.play);
    video.tohide.style.display = 'none';
    video.vid.play();
    video.tohide.removeEventListener('click', video.start);
}

video.tohide.addEventListener('click', video.start);

var scroll = {};
scroll.ids = ['network', 'go-contact'];
scroll.length = scroll.ids.length;
var myvideo = document.getElementsByClassName('onvideo')[0];

scroll.tag = function(id) {
    var element = document.getElementById(id);
    return element;
};

scroll.set = function() {
    var exit = {};
    var i;
    for (i = 0; i < scroll.length; i++) {
        exit[i] = scroll.tag(scroll.ids[i]);
    };
    return exit;
};

scroll.animate = function(tag) {
    if (tag.getBoundingClientRect().top + 50 <= window.innerHeight) {
        tag.classList.add('animate');
    }
    else if (tag.getBoundingClientRect().top < 0 || tag.getBoundingClientRect().top > window.innerHeight) {
        tag.classList.remove('animate');
    }
};

scroll.tags = scroll.set();
// console.log(scroll.tags);

window.addEventListener('scroll', function() {
    var i;
    for (i = 0; i < scroll.length; i++) {
        scroll.animate(scroll.tags[i]);
    }

    // if (window.innerHeight > 800 && window.pageYOffset >= 700 && window.pageYOffset <= 1300) {
    //     myvideo.style.marginTop = -window.pageYOffset + 700 + 'px';
    // }
});

var submit = {}
submit.network = document.getElementById('network');
submit.networkSubmit = document.getElementById('network-submit');
submit.networkEmail = document.getElementById('network-email');


submit.network.addEventListener('click', function showNetwork() {
    submit.networkEmail.style.marginLeft = '0';
    submit.networkEmail.style.width = '100%';
    submit.networkSubmit.style.width = '200px';
    submit.networkSubmit.innerText = 'submit';
    submit.network.removeEventListener('click', showNetwork);

    submit.networkSubmit.addEventListener('click', function sendNetwork() {
        var data = {
            email: submit.networkEmail.value
        };

        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'api/network');
        if (data.email) {
            xhr.send(JSON.stringify(data));
        };

        xhr.onload = function() {
            console.log(xhr.response, data.email);
            if (xhr.response.replace(/"/g, '') === data.email) {
                submit.networkSubmit.removeEventListener('click', sendNetwork)
                submit.networkSubmit.classList.add('done');
                submit.networkSubmit.innerText = 'done';
                submit.networkEmail.style.marginLeft = '-36px';
                submit.networkEmail.style.width = '0';
                submit.networkSubmit.style.width = '100%';

                var data2 = {
                    subject: 'New network member',
                    message: submit.networkEmail.value,
                    email: submit.networkEmail.value
                };

                var xhr2 = new XMLHttpRequest();
                xhr2.open('POST', 'api/contact');
                xhr2.onload = function() {};
                xhr2.send(JSON.stringify(data2));
            }
        };
    });
});

wrap = function() {
    var wrappeds = document.getElementsByClassName('wrapped');
    var wrapper = document.getElementById('wrapper');

    document.getElementById('go-right').addEventListener('click', function() {
        wrapper.appendChild(wrappeds[0]);
    });

    document.getElementById('go-left').addEventListener('click', function() {
        wrapper.insertBefore(wrappeds[5], wrappeds[0]);
    });
    
}();
