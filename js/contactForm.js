var init = function() {
    var form = document.getElementById('contactForm');
    var submit = form.send;

    form.topic.addEventListener('change', function() {
        this.classList.remove('placeholder');
    });

    submit.addEventListener('click', function() {
        var name = form.name;
        var lastname = form.lastname;
        var topic = form.topic;
        var company = form.company;
        var email = form.email;
        var message = form.message;
        var acceptance = form.acceptance;

        var validation = function() {
            var valid = true;
            var i;
            for (i = 0; i < arguments.length; i++) {
                if (arguments[i].classList) arguments[i].classList.remove('error');

                if (arguments[i].validity && arguments[i].validity.valid === false) {
                    arguments[i].classList.add('error');
                    valid = false;
                }
            }
            return valid;
        };

        var resetForm = function() {
            name.value = '';
            lastname.value = '';
            company.value = '';
            topic.value = 'none';
            email.value = '';
            message.value = '';

            topic.classList.add('placeholder');
        };

        if (validation(name, email, message)) {
            var messageText = 'Message from: ' + email.value + '<br/> Subject: ' + topic.value + '<br/> Sender name: ' + name.value + '<br/> Sender last name: ' + lastname.value + '<br/> Company name: ' + company.value + '<br/> Message text: ' + message.value;

            var data = {
                name: name.value,
                subject: topic.value,
                message: messageText,
                email: email.value
            };

            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'api/mail');
            xhr.onload = function() {
                if (xhr.response !== '500') {
                    submit.classList.add('done');
                    submit.removeChild(submit.childNodes[0]);
                    var sent = document.createTextNode('Message sent');
                    submit.appendChild(sent);
                    resetForm();
                }
                else {
                    submit.removeChild(submit.childNodes[0]);
                    var notsent = document.createTextNode('something went wrong, try again later');
                    submit.appendChild(notsent);
                }
            };
            xhr.send(JSON.stringify(data));

            if (acceptance.checked === true) {
                var data = {
                    name: name.value,
                    email: email.value
                };

                var xhr = new XMLHttpRequest();
                xhr.open('POST', 'api/newsletter');
                xhr.send(JSON.stringify(data));
            };
        };
    });        
}();