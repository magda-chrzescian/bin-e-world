var textToHTML = function(text) {
    if (text.indexOf('\n') !== -1 || text.indexOf('\r') !== -1) {
        text = text.replace(/(\n\n|\r\r)/gi,'</p><p>');
        text = text.replace(/(\n|\r)/gi,'<br/>\n');
    }
    else {
        text = text.replace(/(\r\n\r\n)/gi,'</p><p>');
        text = text.replace(/(\r\n)/gi,'<br/>\n\r');
    }
    text = text.replace(/  /gi,'&nbsp;&nbsp;');
    return text;
};

var setVisibility = function() {
    var elements = document.getElementsByClassName('forAdmin');
    var length = elements.length;
    var i;

    for (i = 0; i < length; i++) {
        if (!sessionStorage.auth && elements) {
            elements[i].classList.add('hidden');
        }
        else if (sessionStorage.auth && elements) {
            elements[i].classList.remove('hidden');
        }
    }
};

var setData = function(values, data, template, output, input) {
    var length = values.length;
    var i, id, text, container;

    for (i = 0; i < length; i++) {
        key = values[i];

        switch(values[i]) {
            case 'content':
                content = '<p>' + textToHTML(data.content) + '</p>';
                template.getElementsByClassName('content')[0].innerHTML = content;
                break;

            case 'twitter':
                var link = '...\nSee more here: www.bine.world/posts/';
                var slice = 140 - data.title.length - data.id.length - link.length + 3;
                var content = data.content.replace(/<\/?[^>]+(>|$)/g, '');
                var post = data.title + '\n' + content.slice(0, slice) + link + data.id;
                template.getElementsByClassName('twitter')[0].href = 'https://twitter.com/home?status=' + encodeURI(post);
                break;

            case 'facebook':
                template.getElementsByClassName('facebook')[0].addEventListener('click', function() {
                    FB.ui({
                        picture: 'bine.world/docs/images/bryla.jpg',
                        method: 'feed',
                        link: 'bine.world/posts/' + data.id,
                        name: data.title,
                        caption: 'www.bine.world',
                        description: data.content.replace(/<\/?[^>]+(>|$)/g, ''),
                        message: 'Check out this new post from World of Bin-e!'
                        }, function(response){});
                })   
                break;

            case 'picture':
                if (data.picture) {
                    console.log(data.type);
                    var parent = template.getElementsByClassName('picture')[0];
                    var node = template.createElement('div');
                    node.style.backgroundImage = 'url("' + data.picture + '")';
                    node.style.backgroundSize = data.type;
                    parent.appendChild(node);
                }
                break;

            default:
                text = document.createTextNode(data[key]);
                template.getElementsByClassName(key)[0].appendChild(text);
                container = template.getElementById(input);
        };
    }

    container.id = input + data.id;
    document.getElementById(output).appendChild(container);
};


var getPage = function() {
    var path = window.location.pathname.replace('/', '');
    return path;
}

var myMenu = document.getElementById('nav-table');
var menuButton = document.getElementById('open');
var aciveLink = null;

var openMenu = function(event) {
    if (myMenu.className && myMenu.className === 'visible') {
        myMenu.classList.remove('visible');
    }
    else {
        myMenu.classList.add('visible');
    }

    event.preventDefault();
    event.stopPropagation();

    return false;
};

menuButton.addEventListener('click', function(event) {
    openMenu(event);
});

var setMenus = function() {
    var old = document.referrer || false;
    var path = getPage() || 'index';
    //console.log(path);

    if (path.indexOf('posts') !== -1 && old.indexOf('news') !== -1) {
        activeLink = document.getElementById('news');
    }
    else if (path.indexOf('posts') !== -1 && old.indexOf('blog') !== -1) {
        activeLink = document.getElementById('blog');
    }
    else {
        activeLink = document.getElementById(path);
    }

    if (activeLink) {
      activeLink.classList.add('active');
      activeLink.addEventListener('click', function(event) {
          openMenu(event);
      });
    }

    document.getElementsByTagName('body')[0].addEventListener('click', function() {
        myMenu.classList.remove('visible');
    });

}; setMenus();
