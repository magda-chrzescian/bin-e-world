calculator = {
	config: {
		container: document.getElementById('savings-container'),
		cityList: document.getElementById('city-list'),
		isListOpen: false,
		cityPick: document.getElementById('city-pick'),
		currentCity: false,
		partlyIf: document.getElementById('partly-if'),
		partlyThen: document.getElementById('partly-then'),
		exRate: 1,
		amount: document.getElementById('amount'),
		area: document.getElementById('area'),
		costs: document.getElementById('costs'),
		sorting: document.querySelectorAll('[name=sorting]'),
		partly: document.getElementById('partly'),
		oldCosts: document.getElementById('yearly-costs'),
		newCosts: document.getElementById('bin-e-costs'),
		savings: document.getElementById('savings-value'),
		calculate: document.getElementById('calculate')
	},

	init: function() {
		var self = this;

		this.config.cityPick.addEventListener('keyup', function() {
			self.searchCity(self.config.cityPick.value);
		});

		this.config.cityPick.addEventListener('click', function(event) {
			event.stopPropagation();
			self.showList(!self.config.isListOpen, event);
		});

		this.config.cityList.addEventListener('click', function(event) {
			var tag = event.target;

			if (tag.tagName === 'SPAN') {
				self.getPrices(tag.textContent);
				self.config.cityPick.value = tag.textContent;
				self.getExRate();
			}
		});

		this.config.partlyIf.addEventListener('change', function(event) {
			if (this.children[5].checked) {
				self.config.partlyThen.classList.toggle('undone');
				self.config.partlyThen.children[1].removeAttribute('disabled');
			}
			else {
				self.config.partlyThen.classList.add('undone');
				self.config.partlyThen.children[1].setAttribute('disabled', 'disabled');
				self.config.partlyThen.children[1].value = null;
			}
		});

		this.config.calculate.addEventListener('click', function() {
			self.validate(self.calculate);
		});

		this.getCitiesList();
	},

	getExRate: function() {
		var self = this;
		var exRateReq = new XMLHttpRequest();
		exRateReq.open('GET', 'http://api.fixer.io/latest?base=EUR');
		exRateReq.onload = function() { self.config.exRate = JSON.parse(exRateReq.response).rates; }
		exRateReq.send();
	},

	getPrices: function(city) {
		var self = this;

		var pricesReq = new XMLHttpRequest();
		pricesReq.open('GET', 'api/prices/' + city);
		pricesReq.onload = function() {
			self.config.currentCity = JSON.parse(pricesReq.response).prices[0];	
		}
		pricesReq.send();
	},

	getCitiesList: function() {
		var self = this;

		var citiesReq = new XMLHttpRequest();
		citiesReq.open('GET', 'api/cities');
		citiesReq.onload = function() { 
			self.cities = JSON.parse(citiesReq.response).list;
			self.searchCity('');
		}
		citiesReq.send();
	},

	searchCity: function(value) {
		var tag = this.config.cityList,
			list = this.cities,
			length = list.length,
			text, node,
			i = 0;

		while (tag.firstChild) {
    		tag.removeChild(tag.firstChild);
		}

		for (i; i < length; i++) {
			if (list[i].toLowerCase().indexOf(value.toLowerCase()) !== -1) {
				text = document.createTextNode(list[i]);
				node = document.createElement('span');
				node.className = 'city';
				node.appendChild(text);
				tag.appendChild(node);
			}
		}

	},

	resetSearch: function() {
		var tag = this.config.cityList,
			list = this.cities,
			length = list.length,
			text, node,
			i = 0;

		while (tag.firstChild) {
    		tag.removeChild(tag.firstChild);
		}

		for (i; i < length; i++) {
			text = document.createTextNode(list[i]);
			node = document.createElement('span');
			node.className = 'city';
			node.appendChild(text);
			tag.appendChild(node);
		}
	},

	showList: function(boolean, event) {
		var self = this,
			body = document.getElementsByTagName('body')[0],

			hideList = function(event) {
				var ifTag = function() {
					if (event.target.classList.value === 'city') { return false }
					else if (event.target.classList.contains('city')) { return false }
					else { return true } 
				};

				self.showList(false, event);
				body.removeEventListener('click', hideList);

				if ( ifTag() && !self.config.currentCity) {
					self.config.cityPick.value = null;
					self.resetSearch();
				}
				else if ( ifTag() && self.config.currentCity ) {
					self.config.cityPick.value = self.config.currentCity.city;
					self.resetSearch();
				}
			};

		if (boolean) {
			self.config.cityList.style.display = 'block';
			self.config.isListOpen = true;

			body.addEventListener('click', hideList);
		}
		else {
			self.config.cityList.style.display = 'none';
			self.config.isListOpen = false;

			body.removeEventListener('click', hideList);
		}
	},

	getSortingValue: function(elements) {
		var i = 0,
			length = elements.length,
			value;

		while (i < length) {
			if (elements[i].checked) { value = elements[i].value; }
			i++;
		}

		if (!value) { return false }
		else if (value !== 'partly') { return value }
		else { return this.config.partly.value }
	},

	getAmount: function(costs, waste) {
		var res = costs / this.getPrice(waste);
		return res;
	},

	getCosts: function(amount, waste) {
		var res = this.getPrice(waste) * amount;
		return res;
	},

	getFromArea: function(area, waste) {
		var amount = 0.12 * area;
		var costs = amount * this.getPrice(waste);

		return result = {
			amount: amount,
			costs: costs
		}
	},

	getPrice: function(waste) {
		var res = ((waste.unsorted / 100) * waste.unsortedPrice) + ((waste.sorted / 100) * waste.sortedPrice);
		return res;
	},

	getNewCosts: function(waste, amount) {
		var res = amount / 5 * waste.sortedPrice;
		return res;
	},

	validate: function(callback) {
		if (this.config.cityPick.classList.contains('error')) this.config.cityPick.classList.remove('error');
		if (this.config.partly.classList.contains('error')) this.config.partly.classList.remove('error');
		if (this.config.amount.classList.contains('error')) this.config.amount.classList.remove('error');
		if (this.config.area.classList.contains('error')) this.config.area.classList.remove('error');
		if (this.config.costs.classList.contains('error')) this.config.costs.classList.remove('error');
		if (this.config.partlyIf.classList.contains('error')) this.config.partlyIf.classList.remove('error');

		if (!this.config.currentCity) {
			this.config.cityPick.classList.add('error');
		}
		else if (!this.getSortingValue(this.config.sorting)) {
			this.config.partlyIf.classList.add('error');
		}
		else if (this.getSortingValue(this.config.sorting) < 0 || this.getSortingValue(this.config.sorting) > 100) {
			this.config.partly.classList.add('error');
		}
		else if (this.config.amount.value.length <= 0 && this.config.area.value.length <= 0 && this.config.costs.value.length <= 0) {
			this.config.amount.classList.add('error');
			this.config.area.classList.add('error');
			this.config.costs.classList.add('error');
		}
		else {
			callback.call(this);
		}
	},

	calculate: function() {
		var	exRate, unsortedPrice, sortedPrice, sorted, unsorted, newCosts,
			amount = this.config.amount.value,
			area = this.config.area.value,
			costs = this.config.costs.value,
			sort = this.getSortingValue(this.config.sorting),
			costsContainer = this.config.oldCosts,
			newCostsContainer = this.config.newCosts,
			savingsContainer = this.config.savings;

			this.config.container.removeAttribute('hidden');
			this.config.calculate.innerHTML = 'recalculate';

		this.config.exRate[this.config.currentCity.currency] ? exRate = this.config.exRate[this.config.currentCity.currency] : exRate = 1;

		var waste = {
			unsorted: sort,
			unsortedPrice: this.config.currentCity.unsorted / exRate,
			sorted: 100 - sort,
			sortedPrice: this.config.currentCity.sorted / exRate
		};

		if (costs.length > 0 && amount.length === 0) {							//znam tylko koszty, więc obliczam ilość śmieci potrzebną dalej
			costs = costs * 12;
			amount = this.getAmount(costs, waste);
		}
		else if (costs.length > 0 && amount.length > 0) {
			amount = amount * 12 / 1000;											//znam koszty i ilość, więc nie muszę nic obliczać
			costs = costs * 12;
		}
		else if (costs.length === 0 && amount.length > 0) {	
			amount = amount * 12 / 1000;										//nie znam kosztów, ale znam ilość, więc obliczam koszty potrzebne dalej
			costs = this.getCosts(amount, waste);
		}
		else if (costs.length === 0 && amount.length === 0 && area.length > 0) {					//nie znam kosztów, ani ilości, więc muszę obliczyć jedno i drugie
			var result = this.getFromArea(area, waste);
			costs = result.costs;
			amount = result.amount;
		}

		newCosts = this.getNewCosts(waste, amount);

		costsContainer.innerHTML = costs.toFixed(0) + ' euro';
		newCostsContainer.innerHTML = newCosts.toFixed(0) + ' euro';
		savingsContainer.innerHTML = (costs - newCosts).toFixed(0) + ' euro';
	}
};

calculator.init();