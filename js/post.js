// konfiguracja
var post = {};
post.id = parseInt(window.location.pathname.replace('/posts/', ''));
post.output = 'post';
post.input = 'postView';
post.values = ['date', 'category', 'title', 'content', 'twitter', 'facebook', 'picture'];

// pobieranie pojedynczego posta z bazy danych i wklejanie go do szablonu
post.get = function(template) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '../api/post/' + post.id, true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            post.data = JSON.parse(xhr.response).post[0];
            setData(post.values, post.data, template, post.output, post.input);
        }
    }; xhr.send();
};

// pobieranie szablonu pojedynczego posta
post.xhr = new XMLHttpRequest();
post.xhr.open('GET', '../templates/post.html');
post.xhr.responseType = "document";
post.xhr.onload = function() {
    if (post.xhr.status === 200) {
        post.get(post.xhr.response);
    }
}; post.xhr.send();

setVisibility();
