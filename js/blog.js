// konfiguracja
var posts = {};
posts.output = 'postPrev';
posts.input = 'posts';
posts.values = ['date', 'category', 'title', 'content', 'twitter', 'facebook', 'picture'];

// pobieranie wszystkich postów z bazy danych i wklejanie ich do szablonu
posts.get = function(template) {
    var type = getPage();

    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'api/posts/for' + type, true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            var allPosts = JSON.parse(xhr.response).posts;
            var length = allPosts.length;
            var i, data, t;

            for (i = 0; i < length; i++) {
                data = allPosts[i];
                t = template.cloneNode(true);
                setData(posts.values, data, t, posts.input, posts.output);
            }
        }
    }; xhr.send();
};

// pobieranie szablonu pojedynczego posta
posts.xhr = new XMLHttpRequest();
posts.xhr.open('GET', 'templates/blog.html');
posts.xhr.responseType = "document";
posts.xhr.onload = function() {
    if (posts.xhr.status === 200) {
        posts.get(posts.xhr.response);
    }
}; posts.xhr.send();

//dodawanie listenera, który wyświetli odpowiedniego posta po kliknięciu w niego
document.getElementById(posts.input).addEventListener('click', function(event) {
    if (event.target.classList[0] === 'more') {
        var e = event.target.parentNode;
        
        while (e.classList[0] != 'general') {
            e = e.parentNode;
        }

        var id = e.id.slice(8,e.id.length);
        window.location = 'posts/' + id;
    };
});

setVisibility();
