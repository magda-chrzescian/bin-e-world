var addPost = {};

addPost.getTemplate = function() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'templates/postAdd.html');
    xhr.responseType = "document";

    xhr.onload = function() {
        if (xhr.status === 200) {
            document.getElementById('newPost').appendChild(xhr.response.getElementById('postAdd'));
            var send = document.getElementById('send');
            send.addEventListener('click', addPost.sendPost);
        }
    };

    xhr.send();

    addPost.button.removeEventListener('click', addPost.getTemplate);
    addPost.button.classList.add('done')
};

addPost.button = document.getElementById('add');
addPost.button.addEventListener('click', addPost.getTemplate);

addPost.sendPost = function() {
    var form = document.getElementById('newPostForm');
    var data = {
        author: sessionStorage.auth,
        title: form.title.value,
        content: form.content.value,
        category: form.category.value,
        date: new Date().toISOString().slice(0,10)
    };
    data.fornews = (form.formedia.checked) ? 1 : 0;
    data.forblog = (form.forblog.checked) ? 1 : 0;

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'api/post/add');

    xhr.onload = function() {
        if (xhr.status === 200) {
            window.location = 'blog';
        }
        else if (xhr.status === 401) {
            window.location = 'login';
        }
    };

    xhr.send(JSON.stringify(data));
};