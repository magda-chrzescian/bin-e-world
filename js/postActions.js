post.delete = function() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '../api/postdel/' + post.id, true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            window.location = '../blog';
        }
        else if (xhr.status === 401) {
            window.location = 'login';
        }
    }; xhr.send();
};

post.update = function(title, content, category) {
    var data = {
        author: post.data.author,
        title: title,
        content: content,
        category: category,
        date: post.data.date
    }

    var xhr = new XMLHttpRequest();
    xhr.open('POST', '../api/post/' + post.id, true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            window.location = '../posts/' + post.id;
        }
        else if (xhr.status === 401) {
            window.location = 'login';
        }
    }; xhr.send(JSON.stringify(data));
};

post.edit = function() {
    var edit = document.getElementById('edit');
    edit.removeEventListener('click', post.edit);

    var texts = document.getElementsByClassName('show');
    var lengthh = texts.length;
    var j;
    for (j = 0; j < lengthh; j++) {
        texts[j].classList.add('hidden');
    }

    var title = document.getElementById('title-edit');
    var content = document.getElementById('content-edit');
    var category = document.getElementById('category-edit');

    title.value = post.data.title;
    content.value = post.data.content;
    category.value = post.data.category;

    document.getElementById('title-edit').classList.remove('hidden');
    document.getElementById('content-edit').classList.remove('hidden');
    document.getElementById('category-edit').classList.remove('hidden');

    document.getElementById('delete').classList.add('done');
    edit.removeChild(edit.childNodes[0]);
    var newtext = document.createTextNode('save');
    edit.appendChild(newtext);

    edit.addEventListener('click', function() {
        post.update(title.value, content.value, category.value);
    });
};

document.getElementById('delete').addEventListener('click', post.delete);
document.getElementById('edit').addEventListener('click', post.edit);