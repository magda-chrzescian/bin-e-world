<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Bin-e - World's first intelligent waste bin</title>

@@include('partials/header.html')
<main class="index">
    <section class="header">
        <div class="left-site">
            <header>
                <h1>
                    <span class="subtitle">World's first<br/>intelligent</span><span class="first-title">Waste bin</span>
                </h1>
            </header>
            <div class="benefits-left">
                Bine - the best solution for waste management in smart buildings.
            </div>
        </div>
        <div class="benefits-right right-site">
<!--             <figure>
                <img src="img/bryla.png" alt="bin-e device">
            </figure> -->
        </div>
        <div class="background"></div>
    </section>

    <a class="a-savings" href="http://www.bine.world/savings">
        find out <br/> how much you <br/> can save
    </a>

    <section class="onvideo">
        <div class="brightness">
            <video id="video" controls>
                <source src="img/bine.mp4" type="video/mp4"/>
            </video>
        </div>

        <header id="hide">
            <h4>
                Click and <br/> <strong>see, how it works!</strong>
            </h4>
            <img src="img/play.svg" alt="play">
        </header>
    </section>

    <section class="targets">
        <header>
            <h1>
                <span class="first-title">solution</span><span class="subtitle">designed for all eco-friendly companies</span>
            </h1>
        </header>
        <div class="wrapper">
            <div class="arrow" id="go-left">
                <img src="img/arrow-left.svg">
            </div>
            <ul id="wrapper">
                <li class="wrapped">
                    <img src="../img/airports.png">
                    <div class="point-text">
                        <h2>airports<br/><span style="opacity: 0">.</span></h2>
                    </div>
                </li>
                <li class="wrapped">
                    <img src="../img/public_places.png">
                    <div class="point-text">
                        <h2>public<br/>places</h2>
                    </div>
                </li>
                <li class="wrapped">
                    <img src="../img/smart_offices.png">
                    <div class="point-text">
                        <h2>smart<br/>offices</h2>
                    </div>
                </li>
                <li class="wrapped">
                    <img src="../img/shopping_malls.png">
                    <div class="point-text">
                        <h2>shopping<br/>malls</h2>
                    </div>
                </li>
                <li class="wrapped">
                    <img src="../img/train_station.png">
                    <div class="point-text">
                        <h2>train<br/>stations</h2>
                    </div>
                </li>
                <li class="wrapped">
                    <img src="../img/facility_management.png">
                    <div class="point-text">
                        <h2>facility<br/>management</h2>
                    </div>
                </li>
            </ul>
            <div class="arrow" id="go-right">
                <img src="img/arrow-right.svg">
            </div>
        </div>
    </section>

    <section class="for-u">
        <header>
            <h1>
                <span class="first-title">save your money</span><span class="subtitle">with bine</span>
            </h1>
        </header>
        <div class="left-site">
            <div class="benefits-left">
                <ul>
                    <li>
                        <img class="icon" src="img/increase.svg" width="60px" height="60px">
                        <div class="point-text">
                        <h2>sorting</h2>
                        <p>Increase your company recycling rate and pay lower waste removal fees.</p>
                        </div>
                    </li>
                    <li>
                        <img class="icon" src="img/money.svg" width="60px" height="60px">
                        <div class="point-text">
                        <h2>compression</h2>
                        <p>Pay lower bills and save space for waste storage in your office space.</p>
                        </div>
                    </li>
                    <li>
                        <img class="icon" src="img/management.svg" width="60px" height="60px">
                        <div class="point-text">
                        <h2>management</h2>
                        <p>Reduce required labour and save up to 30% of fuel for waste removal company.</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="benefits-right right-site">
            Bin-e automatically segregates and compresses waste. It reduces labor, time and cost of waste management.
        </div>
        <div class="go" id="network">
            <input id="network-email" type="email" placeholder="enter your email...">
            <button id="network-submit" type="button">Join newsletter</button>
        </div>
        <div class="background"></div>
    </section>

    <section class="for-industry">
        <header>
            <h1>
                <span class="first-title">work eco friendly</span><span class="subtitle">with bine</span>
            </h1>
        </header>
        <div class="left-site">
        <div class="benefits-left">
            <ul>
                <li>
                    <img class="icon" src="img/office.svg" width="60px" height="60px">
                    <div class="point-text">
                        <h2>work space</h2>
                        <p>Make your office space eco-friendly and clean.</p>
                    </div>
                </li>
                <li>
                    <img class="icon" src="img/recycling.svg" width="60px" height="60px">
                    <div class="point-text">
                        <h2>recycling</h2>
                        <p>Increase recycling rate up to 80% and help save the environment.</p>
                    </div>
                </li>
                <li>
                    <img class="icon" src="img/leaf.svg" width="60px" height="60px">
                    <div class="point-text">
                        <h2>saving energy</h2>
                        <p>Bin-e significantly reduces CO<sub>2</sub> footprint associated with wastes utilisation.</p>
                    </div>
                </li>
            </ul>
        </div>
        </div>
        <div class="benefits-right right-site">
            Better segregation with Bin-e means better recycling, and better recycling means a better and cleaner world.
        </div>
<!--         <div class="go" id="network">
            <input id="network-email" type="email" placeholder="enter your email...">
            <button id="network-submit" type="button">Join newsletter</button>
        </div> -->
        <div class="background"></div>
    </section>

    <section class="go-contact" id="go-contact">
        <a href="/contact">
            <header>
                <h1>Contact us</h1>
            </header>
            <p>World of Bin-e is a space for all those who want to create a better world for the future. We are open to investors, partners and the media.</p>
        </a>
    </section>
</main>

<script src="js/index.js"></script>
@@include('partials/footer.html')
