<!doctype html>
<html>
    <head>
        <title>Bin-e - World's first intelligent bin</title>
        <link rel="stylesheet" type="text/css" href="general.css">
        <meta charset="utf-8">
    </head>
    <body class="login">
        <form id="login">
            <h3 style="text-align: center; margin-bottom: 1.2em; letter-spacing: 0.2em;" >log in</h3>
            <div class="form-group">
                <input type="text" placeholder="name" id="name" required>
                <label for="name" hidden>Please enter a valid name</label>
            </div>
            <div class="form-group">
                <input type="password" placeholder="password" id="password" required>
                <label for="password" hidden>Please enter a valid password</label>
            </div>

            <button type="button" id="login">Log in</button>
        </form>

        <button type="button" id="logout">Log out</button>
    <script src="js/login.js"></script>
    </body>
</html>