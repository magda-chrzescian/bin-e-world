<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Bin-e team</title>

@@include('partials/header.html')

<main class="team">
  <div class="list">
    <article class="general">
      <img class="avatar" src="img/kuba.jpg" alt="Jakub Luboński">
      <div>
        <header>
          <h2>Jakub Luboński</h2>
        </header>
        <span>CEO</span>
      </div>
    </article>

    <article class="general">
      <img class="avatar" src="img/marcello.jpg" alt="Marcin Łotysz">
      <div>
        <header>
          <h2>Marcin Łotysz</h2>
        </header>
        <span>CTO</span>
      </div>
    </article>
  </div>

  <div class="list">
    <article>
      <img class="avatar" src="img/marta.jpg" alt="Marta">
      <div>
        <header>
          <h2>Marta</h2>
        </header>
        <span>COO</span>
      </div>
    </article>

    <article>
      <img class="avatar" src="img/ania.jpg" alt="Anna">
      <div>
        <header>
          <h2>Anna</h2>
        </header>
        <span>PR manager</span>
      </div>
    </article>

    <article>
      <img class="avatar" src="img/ola.jpg" alt="Aleksandra">
      <div>
        <header>
          <h2>Aleksandra</h2>
        </header>
        <span>Customer service</span>
      </div>
    </article>

    <article>
      <img class="avatar" src="img/magda.jpg" alt="Magda">
      <div>
        <header>
          <h2>Magda</h2>
        </header>
        <span>App Developer & GUI Designer</span>
      </div>
    </article>

    <article>
      <img class="avatar" src="img/slawek.jpg" alt="Sławomir">
      <div>
        <header>
          <h2>Sławomir</h2>
        </header>
        <span>Head engineer</span>
      </div>
    </article>

    <article>
      <img class="avatar" src="img/borys.jpg" alt="Borys">
      <div>
        <header>
          <h2>Borys</h2>
        </header>
        <span>Software leader</span>
      </div>
    </article>

    <article>
      <img class="avatar" src="img/marek.jpg" alt="Marek">
      <div>
        <header>
          <h2>Marek</h2>
        </header>
        <span>AI engineer</span>
      </div>
    </article>

    <article>
      <img class="avatar" src="img/michal.jpg" alt="Michał">
      <div>
        <header>
          <h2>Michał</h2>
        </header>
        <span>Engineer - object detection</span>
      </div>
    </article>

    <article>
      <img class="avatar" src="img/krzysztof.jpg" alt="Krzysztof">
      <div>
        <header>
          <h2>Krzysztof</h2>
        </header>
        <span>Senior developer</span>
      </div>
    </article>

    <article>
      <img class="avatar" src="img/michal-2.jpg" alt="Michał">
      <div>
        <header>
          <h2>Michał</h2>
        </header>
        <span>Embedded engineer</span>
      </div>
    </article>


    <article></article>
    <article></article>

  </div>
</main>

@@include('partials/footer.html')