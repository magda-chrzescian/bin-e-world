<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Pre-order Bin-e</title>

@@include('partials/header.html')

<main class="order">
	<div class="icons">
		<img src="img/back.svg" id="back" alt="go back">
	</div>

    <div class="go" id="order">
        <input id="order-email" type="email" placeholder="enter your email...">
        <button type="button" id="order-submit">Submit</button>
    </div>

	<div class="comment">
	    <p>Preorder Bin-e now and let us know, that you are interested in cleaning the Earth with us! We will guarantee you special lower price of Bin-e and possibility of buing one of the first devices, right after product line opening.</p>
		<p>Preorder doesn’t obligue you to make a payment, it is just an information for us and further benefits for you.</p>
	</div>
</main>

<script src="js/order.js"></script>

@@include('partials/footer.html')