<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Bin-e blog</title>

@@include('partials/header.html')

<main id="posts" class="blog">
  <header>
    <h1>Blog</h1>
  </header>

  <div class="general forAdmin hidden">
    <button id="add">Add post</button>
  </div>
  <article class="general" id="newPost"></article>
</main>
<div class="break"></div>

<script src="js/blog.js"></script>
<script src="js/addPost.js"></script>

@@include('partials/footer.html')
