<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Contact Bin-e</title>

@@include('partials/header.html')

<main class="contact">
  <header>
    <h1>Contact us</h1>
  </header>

  <article class="general">
    <header class="left">
      <div>
        <span class="h3">Contact</span>
        <h4>Business</h4>
      </div>
    </header>
    <div class="center">
      <span class="bold">Jakub Luboński</span>
      <span>+48 661 111 105</span>
      <span>kuba@bine.world</span>
    </div>
  </article>

  <article class="general">
    <header class="left">
      <div>
        <span class="h3">Contact</span>
        <h4>Technology</h4>
      </div>
    </header>

    <div class="center">
      <span class="bold">Marcin Łotysz</span>
      <span>+48 501 784 522</span>
      <span>marcello@bine.world</span>
    </div>
  </article>

  <article class="general">
    <header class="left">
      <div>
        <span class="h3">Contact</span>
        <h4>for media</h4>
      </div>
    </header>

    <div class="center">
      <span class="bold">Anna Domin</span>
      <span>+48 504 140 661</span>
      <span>ania@bine.world</span>
    </div>
  </article>

  <div class="break"></div>

  <article class="general">
    <header class="left">
      <div>
        <h3>Contact us</h3>
        <span class="h4">Join the world of bin-e</span>
      </div>
    </header>

    <form id="contactForm" class="center">
      <section class="left newsletter">
        <input class="big" type="checkbox" id="acceptance">

        <label for="acceptance">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          width="64px" height="64px" viewBox="0 0 64 64" enable-background="new 0 0 64 64" xml:space="preserve">
          <path  id="checkbox" fill="#FFFFFF" stroke="#d4de56" stroke-width="12" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
          M7.87,32.833 24.37,48.833 55.87,14.833 "/>
          </svg>

          <span class="h4">Sign up</span>
          <span class="h4">for our</span>
          <span class="h4">newsletter</span>
        </label>
      </section>

      <div class="form-group" style="margin-bottom: 20px">
        <input type="email" placeholder="e-mail*" id="email" required>
        <label for="email" hidden>Please enter a valid e-mail address</label>
      </div>
      <div class="form-group">
        <input type="text" placeholder="name*" id="name" required>
        <label for="name" hidden>Please enter a valid name</label>
      </div>
      <div class="form-group">
        <input type="text" placeholder="last name" id="lastname" required>
        <label for="name" hidden>Please enter a valid last name</label>
      </div>
      <div class="form-group" style="margin-bottom: 20px">
        <input type="text" placeholder="company" id="company" required>
        <label for="name" hidden>Please enter a valid company name</label>
      </div>
      <select class="placeholder" placeholder="topic" id="topic">
        <option selected hidden value="none">subject</option>
        <option value="order">order</option>
        <option value="press">press</option>
        <option value="other">other</option>
      </select>
      <div class="form-group">
        <textarea placeholder="message*" id="message" required></textarea>
        <label for="message" hidden>Please enter a valid message</label>
      </div>

      <button type="button" id="send">Send message</button>
    </form>
  </article>

  <div class="break"></div>

  <article class="address general">
    <header class="left">
        <div>
          <h3>Visit us</h3>
          <span class="h4">Our location</span>
        </div>
    </header>

    <address class="center">
      <span class="bold">Poland</span>
      <span>ul. Jasielska 8c,</span>
      <span>60-476 Poznań</span>
    </address>
  </article>

  <div class="break"></div>
</main>

<script src="js/contactForm.js"></script>

@@include('partials/footer.html')
