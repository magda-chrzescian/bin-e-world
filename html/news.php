<!DOCTYPE html>
<html lang="en">
  <head>
    <title>News from Bin-e's World</title>

@@include('partials/header.html')

<main id="posts" class="news">
  <header>
    <h1>News from Bin-e's World</h1>
  </header>

  <div class="general forAdmin hidden">
    <button id="add">Add post</button>
  </div>
  <article class="general" id="newPost"></article>
</main>
<div class="break"></div>

<script src="js/blog.js"></script>
<script src="js/addPost.js"></script>

@@include('partials/footer.html')
