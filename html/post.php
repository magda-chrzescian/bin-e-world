<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Bin-e blog</title>

@@include('partials/header.html')
<main id="post" class="post">
</main>

<div class="main forAdmin hidden">
    <div class="buttons">
        <button id="delete">delete</button>
        <button id="edit">edit</button>
    </div>
</div>

<div class="break"></div>

<script src="../js/post.js"></script>
<script src="../js/postActions.js"></script>

@@include('partials/footer.html')