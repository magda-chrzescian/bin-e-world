<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Calculate your savings</title>

@@include('partials/header.html')
<main class="calculator" id="steps">

    <section class="active">
        <div class="number left">1</div>

        <header>
            <h1>
                <span class="first-title">  Location                            </span>
                <span class="subtitle">     select your location   </span>
            </h1>
        </header>

        <div class="group">
            <div class="form-group">
                <header class="question"> Select your location <sup>*</sup> </header><br/>
                <div class="select">
                    <input type="text" id="city-pick" placeholder="Pick the city">
                    <div class="list" id="city-list" style="display: none"></div>
                </div>
            </div>
        </div>

        <span class="extrainfo">
            <sup>*</sup> If you can not find your city on the list, or your company operates in a specific industry, characterized by non typical distribution of sorts of waste, please <a href="http://www.bine.world/contact">contact us</a>, so we can calculate your savings manually.
        </span>
    </section>

    <section class="active">
        <div class="number left">2</div>

        <header>
            <h1>
                <span class="first-title">  Waste segragation                            </span>
                <span class="subtitle">     informations about waste segregation in your office   </span>
            </h1>
        </header>

        <div class="group">
            <div class="form-group radio" id="partly-if">
                <header class="question"> Are the wastes in your company segregated? </header>
                <input type="radio" name="sorting" value="100"> no <br/>
                <input type="radio" name="sorting" value="0"> yes <br/>
                <input type="radio" name="sorting" value="partly"> partly <br/>
            </div>

            <div class="form-group undone" id="partly-then">
                <header class="question"> Part of waste, that remains unsorted </header>
                <input type="number" disabled="disabled" id="partly" min="1" max="99">
                <span>%</span>
            </div>
        </div>

    </section>

    <section class="active">
        <div class="number left">3</div>

        <header>
            <h1>
                <span class="first-title">  Waste management                                                           </span>
                <span class="subtitle">     Informations about waste management in your office.   </span>
            </h1>
        </header>

        <div class="group">
            <div class="form-group">
                <header class="question"> Amount of waste produced in a month <sup>**</sup> </header>
                <input type="number" id="amount" min="1">
                <span>liters</span>
            </div>

            <div class="form-group">
                <header class="question"> Monthly cost of waste removal in your office <sup>**</sup> </header>
                <input type="number" id="costs" min="1">
                <span>euro</span>
            </div>

            <div class="form-group">
                <header class="question"> Your office area <sup>**</sup> </header>
                <input type="number" id="area" min="1">
                <span>m<sup>2</sup></span>
            </div>
        </div>

        <span class="extrainfo">
            <sup>**</sup> At least one of those fields must be filled.
        </span>
    </section>

    <section class="active savings">
        <div class="number left" style="background-color: #d4de56, color: white">4</div>
        <header>
            <h1>
                <span class="first-title">  Your savings                                                           </span>
                <span class="subtitle">     Find out, how much your company can save using Bine   </span>
            </h1>
        </header>

        <div class="content center" id="savings-container" hidden>
            <div class="form-group">
                <header>Your yearly costs:</header>
                <span id="yearly-costs"></span>
            </div>

            <div class="form-group">
                <header>Yearly costs with Bin-e:</header>
                <span id="bin-e-costs"></span>
            </div>

            <div class="form-group">
                <header style="font-family: OW-800; letter-spacing: 0.05em">Yearly savings:</header>
                <span style="font-family: OW-800; letter-spacing: 0.05em" id="savings-value"></span>
            </div>
        </div>

        <button id="calculate" type="button">calculate</button>
    </section>

</main>

<script src="js/calculator.js"></script>
@@include('partials/footer.html')