<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require 'vendor/autoload.php';

$app = new \Slim\App;

$app->get('/cities', 'getCities');
$app->get('/prices/:city', 'getPrices');
$app->post('/newsletter', 'addNewsletter');
$app->post('/network', 'addNetwork');
$app->post('/orders', 'addOrder');
$app->get('/posts/:type', 'getPosts');
$app->get('/post/:id', 'getPost');
$app->get('/postdel/:id', 'deletePost');
$app->post('/post/:id', 'updatePost');
$app->post('/login', 'logIn');
$app->get('/sessionDestroy', 'destroySession');
$app->post('/post/add', 'addPost');
$app->post('/contact', 'sendRequest');
$app->post('/mail', 'sendMail');

$app->run();

function getCities() {
    $sql = "SELECT city FROM prices";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
        $db = null;
        echo '{"list": ' . json_encode($list) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getPrices($city) {
    $sql = "SELECT * FROM prices WHERE city = :city";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("city", $city);
        $stmt->execute();
        $prices = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"prices": ' . json_encode($prices) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function addNewsletter() {
    $request = Slim::getInstance()->request();
    $newsletter = json_decode($request->getBody());
    $sql = "INSERT INTO newsletter (name, email)
            SELECT * FROM (SELECT :name, :email) AS tmp
            WHERE NOT EXISTS ( SELECT email FROM newsletter WHERE email = :email ) LIMIT 1";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("name", $newsletter->name);
        $stmt->bindParam("email", $newsletter->email);
        $stmt->execute();
        $db = null;
        echo json_encode($newsletter);
    }
    catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function addNetwork() {
    $request = Slim::getInstance()->request();
    $network = json_decode($request->getBody());

    if (!filter_var($network->email, FILTER_VALIDATE_EMAIL) === false) {
        $sql = "INSERT INTO network (email)
                SELECT * FROM (SELECT :email) AS tmp
                WHERE NOT EXISTS ( SELECT email FROM network WHERE email = :email ) LIMIT 1";
        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("email", $network->email);
            $stmt->execute();
            $db = null;
            echo json_encode($network->email);
        }
        catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }
    else {
        echo('400');
    }
}

function addOrder() {
    $request = Slim::getInstance()->request();
    $order = json_decode($request->getBody());

    if (!filter_var($order->email, FILTER_VALIDATE_EMAIL) === false) {
        $sql = "INSERT INTO orders (email)
                SELECT * FROM (SELECT :email) AS tmp
                WHERE NOT EXISTS ( SELECT email FROM orders WHERE email = :email ) LIMIT 1";
        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("email", $order->email);
            $stmt->execute();
            $db = null;
            echo json_encode($order->email);
        }
        catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }
    else {
        echo('400');
    }
}

function getPosts($type) {
    $sql = "SELECT * FROM blog WHERE `" . $type . "` = 1 ORDER BY id DESC";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $posts = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"posts": ' . json_encode($posts) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getPost($id) {
    $sql = "SELECT * FROM blog WHERE id = :id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $post = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"post": ' . json_encode($post) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function addPost() {
    if (isset($_SESSION['user'])) {
        $request = Slim::getInstance()->request();
        $post = json_decode($request->getBody());
        $sql = "INSERT INTO blog (author, category, title, content, date, fornews, forblog, picture) VALUES (:author, :category, :title, :content, :date, :fornews, :forblog, :picture)";
        try {
            $db =  getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("author", $post->author);
            $stmt->bindParam("category", $post->category);
            $stmt->bindParam("title", $post->title);
            $stmt->bindParam("content", $post->content);
            $stmt->bindParam("date", $post->date);
            $stmt->bindParam("fornews", $post->fornews);
            $stmt->bindParam("forblog", $post->forblog);
            $stmt->bindParam("picture", null);
            $stmt->execute();
            $db = null;
            echo json_encode($post);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    } else {
        echo('401');
        exit();
    }
}

function updatePost($id) {
    if (isset($_SESSION['user'])) {
        $request = Slim::getInstance()->request();
        $post = json_decode($request->getBody());
        $sql = "UPDATE blog SET author = :author, category = :category, title = :title, content = :content, date = :date WHERE id = :id";
        try {
            $db =  getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("author", $post->author);
            $stmt->bindParam("category", $post->category);
            $stmt->bindParam("title", $post->title);
            $stmt->bindParam("content", $post->content);
            $stmt->bindParam("date", $post->date);
            $stmt->bindParam("id", $id);
            $stmt->execute();
            $db = null;
            echo json_encode($post);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    } else {
        echo('401');
        exit();
    }
}

function deletePost($id) {
    if (isset($_SESSION['user'])) {
        $sql = "DELETE FROM blog WHERE id=:id";
        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("id", $id);
            $stmt->execute();
            $db = null;
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    } else {
        echo('401');
        exit();
    }
}

function logIn() {
    $request = Slim::getInstance()->request();
    $user = json_decode($request->getBody());
    $sql = "SELECT name FROM users WHERE name = :name AND password = :password";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("password", $user->password);
        $stmt->bindParam("name", $user->name);
        $stmt->execute();
        $response = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        if ($response) {
            $_SESSION['user'] = $response[0]->name;
            echo json_encode($response[0]->name);
        }
        else {
          echo '401';
        }
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function sendMail() {
    $request = Slim::getInstance()->request();
    $mail = json_decode($request->getBody(), true);
    $to = 'magda.chrzescian@grupalobo.pl';
    $subject = $mail['subject'];
    $message = $mail['message'];
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=Windows-1250' . "\r\n";
    $headers .= 'Content-Transfer-Encoding: 8bit' . "\r\n";
    $headers .= 'Reply-To: ' . $mail['email'] . "\r\n";
    $from = '-f magda.chrzescian@grupalobo.pl';

    if (mail($to, $subject, $message, $headers, $from)) {
        echo('200');
    } else {
        echo('500');
    }
}

function sendRequest() {
    $request = Slim::getInstance()->request();
    $mail = json_decode($request->getBody(), true);
    $to = 'ola@bine.world';
    $subject = $mail['subject'];
    $message = $mail['message'];
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= 'Content-Transfer-Encoding: 8bit' . "\r\n";
    $headers .= 'Reply-To: ' . $mail['email'] . "\r\n";
    $from = '-f info@bine.world';

    if (mail($to, $subject, $message, $headers, $from)) {
        echo('400');
    } else {
        echo('500');
    }
}

function destroySession() {
    session_destroy();
}

function getConnection() {
    $dbhost = "localhost:8889";
    $dbuser = "root";
    $dbpass = "root";
    $dbname = "bin_e";
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}

?>
